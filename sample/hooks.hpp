/*
 *  Copyright (C) 2012-2013 Timothée Feuillet
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Lesser General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Lesser General Public License for more details.
 *
 *  You should have received a copy of the GNU Lesser General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

//
// file : hooks.hpp
// in : file:///home/feuill_t/project_y3/cr::map/sample/hooks.hpp
//
// created by : Timothée Feuillet on fus-ro-dah.site
// date: 06/03/2013 23:35:52
//

#ifndef __HOOKS_HPP__
#define __HOOKS_HPP__

#include <map>

#include <utility.hpp>
#include <chunk.hpp>

// a class that create a particles for a special block
class particles_for_block : public cr::special_block
{
  public:
    particles_for_block(Ogre::SceneManager *csmgr, const std::string &_name) : smgr(csmgr), name(_name) {}
    virtual ~particles_for_block() {}

    virtual void on_hidden(unsigned char bck, const Ogre::Vector3 &pos)
    {
      if (nodes.count(pos))
      {
        smgr->getRootSceneNode()->removeAndDestroyChild(nodes.at(pos)->getName());
        nodes.erase(pos);
      }
    }

    virtual void on_visible(unsigned char bck, const Ogre::Vector3 &pos)
    {
      if (!nodes.count(pos))
      {
        nodes[pos] = smgr->getRootSceneNode()->createChildSceneNode();
        nodes[pos]->setPosition(pos + 0.5_x + 0.5_y + 0.5_z);
        nodes[pos]->attachObject(smgr->createParticleSystem("SmokeBlock:" + Ogre::StringConverter::toString(pos), name));
      }
    }

  private:
    std::map<Ogre::Vector3, Ogre::SceneNode *, vector_cmp> nodes;
    Ogre::SceneManager *smgr;
    std::string name;
};

#endif /*__HOOKS_HPP__*/

// kate: indent-mode cstyle; indent-width 2; replace-tabs on; 
