/*
 *  Copyright (C) 2012-2013 Timothée Feuillet
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Lesser General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Lesser General Public License for more details.
 *
 *  You should have received a copy of the GNU Lesser General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "map.hpp"
#include <utility.hpp>

chochana::map::map(Ogre::SceneManager *mSceneMgr)
  : wsz(768, 128, 768)
{
  crmap = new cr::chunk_map(wsz, "MapBasic", mSceneMgr);

  // additionnal materials
  crmap->set_additional_material("TransparentBlock", {163, 146}, true);
  crmap->set_additional_material("TransparentBlock2", {18}, true);
  crmap->set_additional_material("Block10", {10}, false);

  // multi-textures blocks
  crmap->set_block_textures(1, {3, 1, 4});
}

chochana::map::~map()
{
}

cr::chunk_map *chochana::map::get_crmap()
{
  return crmap;
}

void chochana::map::build()
{
  int map[] =
  {
    1, 2, 2, 2, 2, 17, 17, 17, 101, 102
  };

  for (int z = 0; z < wsz.z; ++z)
  {
    for (int x = 0; x < wsz.x; ++x)
    {
      float Height = cosf((float)(x) / (80.0)) * sinf((float)(z) / (80.0)) * wsz.y / 3 + wsz.y / 2;

      for (int y = 0; y < Height && y < wsz.y; ++y)
      {
        if ((int)Height == y && y < 30)
          crmap->set_block(18, Ogre::Vector3(x, 30, z));
        else if ((int)Height == y && y > 100 && 100.0_rand < 85.0)
          crmap->set_block(146, Ogre::Vector3(x, 120, z));
        else if ((int)Height == y && y >= 42 && y <= 44)
        {
          crmap->set_block(10, Ogre::Vector3(x, 60 + y - 42, z));
          crmap->set_block(10, Ogre::Vector3(x, 60 - (y - 42), z));
        }

        crmap->set_block(map[(rand() % (sizeof(map) / sizeof(int)))], Ogre::Vector3(x, y, z));
      }
    }
  }

  // 263 52 225 --> hole
  for (int z = 225; z < wsz.z && z < 550; ++z)
  {
    for (int x = 250; x < wsz.x && x < 300; ++x)
    {
      for (int y = 52; y < wsz.y && y < 82; ++y)
      {
        if (sqrt((x - 275) * (x - 275) + (y - 67) * (y - 67)) < 15)
        {
          if (100.0_rand < 0.03f)
            crmap->set_block(163, Ogre::Vector3(x, y, z));
          else
            crmap->set_block(0, Ogre::Vector3(x, y, z));
        }
      }
    }
  }
  crmap->update();
}
// kate: indent-mode cstyle; indent-width 2; replace-tabs on; 
