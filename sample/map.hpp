/*
 *  Copyright (C) 2012-2013 Timothée Feuillet
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Lesser General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Lesser General Public License for more details.
 *
 *  You should have received a copy of the GNU Lesser General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#ifndef CR_MAP_HPP
#define CR_MAP_HPP

#include <Ogre.h>

#include "chunk.hpp"

namespace chochana
{
  class map
  {
    public:
      map(Ogre::SceneManager *mSceneMgr);
      ~map();

      void build();

      cr::chunk_map *get_crmap();

      Ogre::Vector3 get_world_size()
      {
        return wsz;
      }

    private:
      Ogre::Vector3 wsz;
      cr::chunk_map *crmap;
  };
} // namespace chochana


#endif /*CR_MAP_HPP*/
// kate: indent-mode cstyle; indent-width 2; replace-tabs on; 
