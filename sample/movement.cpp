/*
 *  Copyright (C) 2012-2013 Timothée Feuillet
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Lesser General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Lesser General Public License for more details.
 *
 *  You should have received a copy of the GNU Lesser General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "BaseApplication.h"
#include "movement.hpp"
#include "utility.hpp"

wasd::wasd (BaseApplication *cba, const OIS::KeyCode &cforward, const OIS::KeyCode &cbackward,
            const OIS::KeyCode &csleft, const OIS::KeyCode &csright)
  : ba(cba), forward(cforward), backward(cbackward), sleft(csleft), sright(csright), dlm(false),
    angle_deg(Ogre::Vector3::ZERO), direction(Ogre::Vector3::ZERO), normalised_direction(Ogre::Vector3::ZERO), oriented_direction(Ogre::Vector3::ZERO)
{
  ba->register_keyboard_receiver(this);
  ba->register_mouse_receiver(this);
}

wasd::~wasd()
{
  ba->unregister_keyboard_receiver(this);
  ba->unregister_mouse_receiver(this);
}

void wasd::update_oriented_direction()
{
  // fuck Ogre vectors and their lack of 'rotate'.
  float x = normalised_direction.x;
  float y = normalised_direction.y;
  float z = normalised_direction.z;

  // FIXME: couldn't it be shorter ?
  Ogre::Vector3 tmp;
  Ogre::Vector3 angle(Ogre::Degree(angle_deg.x).valueRadians(), 0, 0);

  tmp.x = x;
  tmp.z = z;
  x = tmp.x * cos(angle.y) - tmp.z * sin(angle.y);
  z = tmp.x * sin(angle.y) + tmp.z * cos(angle.y);
  tmp.y = y;
  tmp.z = z;
  y = tmp.y * cos(angle.x) - tmp.z * sin(angle.x);
  z = tmp.y * sin(angle.x) + tmp.z * cos(angle.x);
  tmp.x = x;
  tmp.y = y;
  x = tmp.x * cos(angle.z) - tmp.y * sin(angle.z);
  y = tmp.x * sin(angle.z) + tmp.y * cos(angle.z);

  angle = Ogre::Vector3(0, -Ogre::Degree(angle_deg.y).valueRadians(), 0);

  tmp.y = y;
  tmp.z = z;
  y = tmp.y * cos(angle.x) - tmp.z * sin(angle.x);
  z = tmp.y * sin(angle.x) + tmp.z * cos(angle.x);
  tmp.x = x;
  tmp.z = z;
  x = tmp.x * cos(angle.y) - tmp.z * sin(angle.y);
  z = tmp.x * sin(angle.y) + tmp.z * cos(angle.y);
  tmp.x = x;
  tmp.y = y;
  x = tmp.x * cos(angle.z) - tmp.y * sin(angle.z);
  y = tmp.x * sin(angle.z) + tmp.y * cos(angle.z);

  oriented_direction = Ogre::Vector3(x, y, z);
}

bool wasd::keyPressed (const OIS::KeyEvent &arg)
{
  if (arg.key == forward)
    direction.z = -1;
  else if (arg.key == backward)
    direction.z = 1;
  else if (arg.key == sleft)
    direction.x = -1;
  else if (arg.key == sright)
    direction.x = 1;

  normalised_direction = direction.normalisedCopy();
  if (normalised_direction.isNaN())
    normalised_direction = 0.0_z;
  if (!dlm)
    update_oriented_direction();
  return true;
}

bool wasd::keyReleased (const OIS::KeyEvent &arg)
{
  if (arg.key == forward && direction.z == -1)
    direction.z = 0;
  else if (arg.key == backward && direction.z == 1)
    direction.z = 0;
  else if (arg.key == sleft && direction.x == -1)
    direction.x = 0;
  else if (arg.key == sright && direction.x == 1)
    direction.x = 0;

  normalised_direction = direction.normalisedCopy();
  if (normalised_direction.isNaN())
    normalised_direction = 0.0_x;
  if (!dlm)
    update_oriented_direction();
  return true;
}

bool wasd::mouseMoved (const OIS::MouseEvent &arg)
{
  if (dlm)
    return true;
  Ogre::Vector3 td (arg.state.Y.rel, arg.state.X.rel, 0);
  angle_deg.y += td.y * -0.2;
  while (angle_deg.y < 360)
    angle_deg.y += 360;
  while (angle_deg.y > 360)
    angle_deg.y -= 360;
  angle_deg.x += td.x * -0.2;
  while (angle_deg.x < 360)
    angle_deg.x += 360;
  while (angle_deg.x > 360)
    angle_deg.x -= 360;
  update_oriented_direction();
  return true;
}

