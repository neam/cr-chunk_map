/*
 *  Copyright (C) 2012-2013 Timothée Feuillet
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Lesser General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Lesser General Public License for more details.
 *
 *  You should have received a copy of the GNU Lesser General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef __MOVEMENT_HPP__
#define __MOVEMENT_HPP__

#include <OISMouse.h>
#include <OISKeyboard.h>
#include <OgreVector3.h>

class BaseApplication;
// player movement
class wasd : public OIS::MouseListener, public OIS::KeyListener
{
  public:
    wasd(BaseApplication *cba, const OIS::KeyCode &cforward = OIS::KC_W, const OIS::KeyCode &cbackward = OIS::KC_S,
          const OIS::KeyCode &csleft = OIS::KC_A, const OIS::KeyCode &csright = OIS::KC_D);

    ~wasd();

    void disable_mouse_listener(bool disable = true)
    {
      dlm = disable;
    }

    bool is_mouse_listener_enabled() const
    {
      return dlm;
    }

    Ogre::Vector3 get_direction() const
    {
      return normalised_direction;
    }

    Ogre::Vector3 get_oriented_direction() const
    {
      return oriented_direction;
    }

    Ogre::Vector3 get_camera_direction() const
    {
      return angle_deg;
    }

    virtual bool keyPressed (const OIS::KeyEvent &arg);
    virtual bool keyReleased (const OIS::KeyEvent &arg);
    virtual bool mouseMoved (const OIS::MouseEvent &arg);

    virtual bool mousePressed (const OIS::MouseEvent &arg, OIS::MouseButtonID id){}
    virtual bool mouseReleased (const OIS::MouseEvent &arg, OIS::MouseButtonID id){}

  private:
    void update_oriented_direction();

  private:
    BaseApplication *ba;
    OIS::KeyCode forward;
    OIS::KeyCode backward;
    OIS::KeyCode sleft;
    OIS::KeyCode sright;
    bool dlm;
    Ogre::Vector3 angle_deg;
    Ogre::Vector3 direction;
    Ogre::Vector3 normalised_direction;
    Ogre::Vector3 oriented_direction;
};

#endif /*__MOVEMENT_HPP__*/
