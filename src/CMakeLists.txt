##
## CR::MAP
##

cmake_minimum_required(VERSION 2.8)

set(sources ./chunk.cpp)
set(headers ./chunk.hpp ./astar.hpp)

add_library(cr_map STATIC ${sources} ${headers})
