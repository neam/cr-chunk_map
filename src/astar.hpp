/*
 *  Copyright (C) 2012-2013 Timothée Feuillet
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Lesser General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Lesser General Public License for more details.
 *
 *  You should have received a copy of the GNU Lesser General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
//
// file : astar.hpp
// in : file:///home/feuill_t/project_y3/pfa/chochana/src/map/astar.hpp
//
// created by : Timothée Feuillet on fus-ro-dah.site
// date: 06/03/2013 17:24:54
//

#ifndef __ASTAR_HPP__
#define __ASTAR_HPP__

#include <map>
#include <set>
#include <boost/unordered_map.hpp>
#include <boost/unordered_set.hpp>
#include "utility.hpp"

#include <OgreVector3.h>

#include "chunk.hpp"

namespace cr
{
  // AstarDriver helpers
  template<size_t RmgFactor = 1, size_t Divisor = 1>
  class basic_astar_driver
  {
    public:
      static float heuristic_score(const Ogre::Vector3 &to, const Ogre::Vector3 &pt)
      {
        return pt.squaredDistance(to) * static_cast<float>(RmgFactor) / static_cast<float>(Divisor);
      };

    public:
      const static Ogre::Vector3 move_pos[8];
  };

  template<size_t RmgFactor, size_t Divisor>
  const Ogre::Vector3 basic_astar_driver<RmgFactor, Divisor>::move_pos[8] =
  {
    {0, 0, -1},
    {0, 0, 1},
    { -1, 0, 0},
    {1, 0, 0},
    { -1, 0, -1},
    { -1, 0, 1},
    {1, 0, -1},
    {1, 0, 1},
  };

  // just to get some 'random' paths (breaks the lines)
  template<size_t RmgFactor, size_t RandFactor, size_t Divisor = 1>
  class random_astar_driver
  {
    public:
      static float heuristic_score(const Ogre::Vector3 &to, const Ogre::Vector3 &pt)
      {
        return pt.squaredDistance(to) * static_cast<float>(RmgFactor) / static_cast<float>(Divisor)
        + 1.0_rand * static_cast<float>(RandFactor) / static_cast<float>(Divisor); // insert a random to break the paths
      };

    public:
      const static Ogre::Vector3 move_pos[8];
  };

  template<size_t RmgFactor, size_t RandFactor, size_t Divisor>
  const Ogre::Vector3 random_astar_driver<RmgFactor, RandFactor, Divisor>::move_pos[8] =
  {
    {0, 0, -1},
    {0, 0, 1},
    { -1, 0, 0},
    {1, 0, 0},
    { -1, 0, -1},
    { -1, 0, 1},
    {1, 0, -1},
    {1, 0, 1},
  };

  // Astar path finding
  template<typename AstarDriver>
  class astar
  {
    private:
      astar() = delete;
      ~astar() = delete;

    public:
      //--> en.wikipedia.org/wiki/A*_search_algorithm
      // implements an A*. 'max_height' is the maximum height that could be climbed.
      // it return a list of waypoints
      // NOTE: the path is only valable at the time of the computation. don't forget to check it continuously.
      // return false if no path could be found. (else ret is the list of pos ]from;to] and the function return true)
      template<typename ReturnContainer = std::list<Ogre::Vector3>>
      static bool path_finder(cr::chunk_map *map, const Ogre::Vector3 &from, const Ogre::Vector3 &to,
                              ReturnContainer &ret, int max_height)
      {
        // funcs //
        // return if the move is possible and update the value of dsp
        auto get_new_position = [&](Ogre::Vector3 & dsp, const Ogre::Vector3 & old_pos) -> bool
        {
          dsp += old_pos;

          try
          {
            if (map->get_block(dsp) != 0)
            {
              int _dsp_y = map->get_upper_ground_level(dsp.x, dsp.y, dsp.z) + 1;
              if (_dsp_y - dsp.y > max_height)
                return false;
              dsp.y = _dsp_y;
            }
            else while (map->get_block(dsp - 1.0_y) == 0) // air, go down. (also called gravity, sometimes)
                --dsp.y;
          }
          catch (...) // out of the map
          {
            return false;
          }
          return true;
        };

        // A* containers
        ret.clear();

        std::map<float, std::list<Ogre::Vector3>> opened;
        boost::unordered_set<Ogre::Vector3, vector_cmp> openset( {from});
        boost::unordered_set<Ogre::Vector3, vector_cmp> closed;
        std::map<Ogre::Vector3, float, vector_cmp> g_score( {{from, 0}});
        std::map<Ogre::Vector3, float, vector_cmp> f_score( {{from, g_score[from] + AstarDriver::heuristic_score(to, from)}});
        std::map<Ogre::Vector3, Ogre::Vector3, vector_cmp> path_map;

        opened[f_score[from]].push_back(from);

        while (!opened.empty())
        {
          auto begin = opened.begin();
          float weight = begin->first;
          std::list<Ogre::Vector3> &_lst = begin->second;
          Ogre::Vector3 current = _lst.front();
          _lst.pop_front();

          if (_lst.empty())
            opened.erase(weight);

          openset.erase(current);

          if (closed.count(current))
            continue;

          closed.insert(current);

          if (current == to) // we got it !! [reconstruct the path]
          {
            ret.push_front(to);
            while (path_map.count(ret.front()) && ret.front() != from)
              ret.push_front(path_map[ret.front()]);
            return true;
          }

          for (Ogre::Vector3 newpos : AstarDriver::move_pos)
          {
            //float dst = newpos.squaredLength();

            if (!get_new_position(newpos, current))
              continue;
            if (closed.count(newpos))
              continue;

            float dst = newpos.squaredDistance(current);

            float ng_score = g_score[current] + dst;

            bool cnt = openset.count(newpos);
            if (!cnt || ng_score < g_score[newpos])
            {
              path_map[newpos] = current;
              g_score[newpos] = ng_score;
              float nf_score = ng_score + AstarDriver::heuristic_score(to, newpos);
              float of_score = f_score[newpos];
              f_score[newpos] = nf_score;

              if (!cnt)
              {
                opened[nf_score].push_back(newpos);
                openset.insert(newpos);
              }
              else
              {
                opened[of_score].remove(newpos);
                if (opened[of_score].empty())
                  opened.erase(of_score);
                opened[nf_score].push_back(newpos);
              }
            }
          }
        }
        return false;
      }

    private:
  };
} // namespace cr

#endif /*__ASTAR_HPP__*/
// kate: indent-mode cstyle; indent-width 2; replace-tabs on; 
