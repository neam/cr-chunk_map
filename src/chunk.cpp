/*
 *  Copyright (C) 2012-2013 Timothée Feuillet
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Lesser General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Lesser General Public License for more details.
 *
 *  You should have received a copy of the GNU Lesser General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <string>
#include <cmath>
#include <queue>

#include <OgreStringConverter.h>

#include "chunk.hpp"
#include "utility.hpp"

#include <boost/unordered_set.hpp>
#include <boost/unordered_map.hpp>

// CHUNK NESTED CLASS
cr::chunk_map::chunk::chunk ()
: map{{{0}}}, dirty (true), empty (true)
{
}

void cr::chunk_map::chunk::init (Ogre::SceneNode *croot, const Ogre::Vector3 &pos, cr::chunk_map *d)
{
  root = croot;
  position = pos;
  mprt = d;
}

cr::chunk_map::chunk::~chunk()
{
  for (auto &i : meshes) // cleanup
  {
    root->detachObject (i.second);
    delete i.second;
  }
  meshes.clear();
}

void cr::chunk_map::chunk::upd_fnc(boost::unordered_map<long, int> &ivertex, size_t estimate_count, block current_block, block &current_tid, Ogre::String &current_mat, block idx)
{
  current_tid = mprt->get_textures_ids(current_block, idx);
  current_mat = mprt->get_material_name(current_tid);
  if (!meshes.count(current_mat) || !meshes[current_mat]) // not already registered
  {
    auto msh = new Ogre::ManualObject("cr::chunk_map::chunk::(" + current_mat + ")mesh_chunk@" + Ogre::StringConverter::toString((size_t) this));
    meshes[current_mat] = msh;
    root->attachObject(meshes[current_mat]);
    ivertex[(long)meshes[current_mat]] = 0;
    //meshes[current_mat]->setDynamic(true);
    msh->begin(current_mat);
    msh->estimateIndexCount(estimate_count);
    msh->estimateVertexCount(estimate_count);
  }
}

void cr::chunk_map::chunk::update()
{
  if (!dirty)
    return;
  dirty = false;

  boost::unordered_map<long, int> ivertex;

  size_t estimate_count = mprt->chunk_size * mprt->chunk_size * (mprt->chunk_size / 4);

  for (auto &i : meshes) // cleanup + init
  {
    i.second->clear();
    i.second->begin(i.first);
    //i.second->beginUpdate(0);
    i.second->estimateIndexCount(estimate_count);
    i.second->estimateVertexCount(estimate_count);
    ivertex[(long)i.second] = 0;
  }

  block current_block;
  block block_tmp;

  /* Only create visible faces of each chunk */
  block default_block = 1;
  int SX = 0;
  int SY = 0;
  int SZ = 0;
  float V1;
  float V2;
  float U1;
  float U2;
  Ogre::Vector3 tmp_pos = position * chunk_size;

  for (int z = 0; z < chunk_size; ++z)
  {
    for (int y = 0; y < chunk_size; ++y)
    {
      for (int x = 0; x < chunk_size; ++x)
      {
        current_block = map[x][y][z];
        if (current_block == 0) continue;
        bool visible = false;

        bool has_texture_ids = mprt->has_textures_ids(current_block);

        int *default_ivertex = nullptr;
        Ogre::ManualObject *default_mesh_chunk = nullptr;

        block current_tid = 0;
        Ogre::String current_mat;

        // the default one
        upd_fnc(ivertex, estimate_count, current_block, current_tid, current_mat, 2);
        default_mesh_chunk = meshes[current_mat];
        default_ivertex = &ivertex[(long)default_mesh_chunk];

        //x-1
        block_tmp = default_block;
        if (x > 0) block_tmp = map[x - 1][y][z];
        else block_tmp = mprt->get_near_block (this, -1, y, z);

        if (mprt->is_displayable(block_tmp))
        {
          U1 = 0.0625f * (float) ( (current_tid - 1) % 16);
          U2 = U1 + 0.0625f;
          V1 = 0.0625f * (float) ( (current_tid - 1) / 16);
          V2 = V1 + 0.0625f;

          default_mesh_chunk->position (tmp_pos + Ogre::Vector3 (x, y,   z + 1));
          default_mesh_chunk->normal (-1, 0, 0);
          default_mesh_chunk->textureCoord (U2, V2);
          default_mesh_chunk->position (tmp_pos + Ogre::Vector3 (x, y + 1, z + 1));
          default_mesh_chunk->normal (-1, 0, 0);
          default_mesh_chunk->textureCoord (U2, V1);
          default_mesh_chunk->position (tmp_pos + Ogre::Vector3 (x, y + 1, z));
          default_mesh_chunk->normal (-1, 0, 0);
          default_mesh_chunk->textureCoord (U1, V1);
          default_mesh_chunk->position (tmp_pos + Ogre::Vector3 (x, y,   z));
          default_mesh_chunk->normal (-1, 0, 0);
          default_mesh_chunk->textureCoord (U1, V2);

          default_mesh_chunk->triangle ((*default_ivertex), (*default_ivertex) + 1, (*default_ivertex) + 2);
          default_mesh_chunk->triangle ((*default_ivertex) + 2, (*default_ivertex) + 3, (*default_ivertex));

          (*default_ivertex) += 4;
          visible = true;
        }

        //x+1
        block_tmp = default_block;
        if (x < chunk_size - 1) block_tmp = map[x + 1][y][z];
        else block_tmp = mprt->get_near_block (this, x + 1, y, z);

        if (mprt->is_displayable(block_tmp))
        {
          if (has_texture_ids)
          {
            upd_fnc(ivertex, estimate_count, current_block, current_tid, current_mat, 3);
            default_mesh_chunk = meshes[current_mat];
            default_ivertex = &ivertex[(long)default_mesh_chunk];
          }

          U1 = 0.0625f * (float) ( (current_tid - 1) % 16);
          U2 = U1 + 0.0625f;
          V1 = 0.0625f * (float) ( (current_tid - 1) / 16);
          V2 = V1 + 0.0625f;

          default_mesh_chunk->position (tmp_pos + Ogre::Vector3 (x + 1, y,   z));
          default_mesh_chunk->normal (1, 0, 0);
          default_mesh_chunk->textureCoord (U2, V2);
          default_mesh_chunk->position (tmp_pos + Ogre::Vector3 (x + 1, y + 1, z));
          default_mesh_chunk->normal (1, 0, 0);
          default_mesh_chunk->textureCoord (U2, V1);
          default_mesh_chunk->position (tmp_pos + Ogre::Vector3 (x + 1, y + 1, z + 1));
          default_mesh_chunk->normal (1, 0, 0);
          default_mesh_chunk->textureCoord (U1, V1);
          default_mesh_chunk->position (tmp_pos + Ogre::Vector3 (x + 1, y,   z + 1));
          default_mesh_chunk->normal (1, 0, 0);
          default_mesh_chunk->textureCoord (U1, V2);

          default_mesh_chunk->triangle ((*default_ivertex), (*default_ivertex) + 1, (*default_ivertex) + 2);
          default_mesh_chunk->triangle ((*default_ivertex) + 2, (*default_ivertex) + 3, (*default_ivertex));

          (*default_ivertex) += 4;
          visible = true;
        }

        //y-1
        block_tmp = default_block;
        if (y > 0) block_tmp = map[x][y - 1][z];
        else block_tmp = mprt->get_near_block (this, x, -1, z);

        if (mprt->is_displayable(block_tmp))
        {
          if (has_texture_ids)
          {
            upd_fnc(ivertex, estimate_count, current_block, current_tid, current_mat, 0);
            default_mesh_chunk = meshes[current_mat];
            default_ivertex = &ivertex[(long)default_mesh_chunk];
          }

          U1 = 0.0625f * (float) ( (current_tid - 1) % 16);
          U2 = U1 + 0.0625f;
          V1 = 0.0625f * (float) ( (current_tid - 1) / 16);
          V2 = V1 + 0.0625f;

          default_mesh_chunk->position (tmp_pos + Ogre::Vector3 (x,   y, z));
          default_mesh_chunk->normal (0, -1, 0);
          default_mesh_chunk->textureCoord (U1, V2);
          default_mesh_chunk->position (tmp_pos + Ogre::Vector3 (x + 1, y, z));
          default_mesh_chunk->normal (0, -1, 0);
          default_mesh_chunk->textureCoord (U2, V2);
          default_mesh_chunk->position (tmp_pos + Ogre::Vector3 (x + 1, y, z + 1));
          default_mesh_chunk->normal (0, -1, 0);
          default_mesh_chunk->textureCoord (U2, V1);
          default_mesh_chunk->position (tmp_pos + Ogre::Vector3 (x,   y, z + 1));
          default_mesh_chunk->normal (0, -1, 0);
          default_mesh_chunk->textureCoord (U1, V1);

          default_mesh_chunk->triangle ((*default_ivertex), (*default_ivertex) + 1, (*default_ivertex) + 2);
          default_mesh_chunk->triangle ((*default_ivertex) + 2, (*default_ivertex) + 3, (*default_ivertex));

          (*default_ivertex) += 4;
          visible = true;
        }

        //y+1
        block_tmp = default_block;
        if (y < chunk_size - 1) block_tmp = map[x][y + 1][z];
        else block_tmp = mprt->get_near_block (this, x, y + 1, z);

        if (mprt->is_displayable(block_tmp))
        {
          if (has_texture_ids)
          {
            upd_fnc(ivertex, estimate_count, current_block, current_tid, current_mat, 1);
            default_mesh_chunk = meshes[current_mat];
            default_ivertex = &ivertex[(long)default_mesh_chunk];
          }

          U1 = 0.0625f * (float) ( (current_tid - 1) % 16);
          U2 = U1 + 0.0625f;
          V1 = 0.0625f * (float) ( (current_tid - 1) / 16);
          V2 = V1 + 0.0625f;

          default_mesh_chunk->position (tmp_pos + Ogre::Vector3 (x,   y + 1, z + 1));
          default_mesh_chunk->normal (0, 1, 0);
          default_mesh_chunk->textureCoord (U2, V1);
          default_mesh_chunk->position (tmp_pos + Ogre::Vector3 (x + 1, y + 1, z + 1));
          default_mesh_chunk->normal (0, 1, 0);
          default_mesh_chunk->textureCoord (U2, V2);
          default_mesh_chunk->position (tmp_pos + Ogre::Vector3 (x + 1, y + 1, z));
          default_mesh_chunk->normal (0, 1, 0);
          default_mesh_chunk->textureCoord (U1, V2);
          default_mesh_chunk->position (tmp_pos + Ogre::Vector3 (x,   y + 1, z));
          default_mesh_chunk->normal (0, 1, 0);
          default_mesh_chunk->textureCoord (U1, V1);

          default_mesh_chunk->triangle ((*default_ivertex), (*default_ivertex) + 1, (*default_ivertex) + 2);
          default_mesh_chunk->triangle ((*default_ivertex) + 2, (*default_ivertex) + 3, (*default_ivertex));

          (*default_ivertex) += 4;
          visible = true;
        }

        //z-1
        block_tmp = default_block;
        if (z > 0) block_tmp = map[x][y][z - 1];
        else block_tmp = mprt->get_near_block (this, x, y, -1);

        if (mprt->is_displayable(block_tmp))
        {
          if (has_texture_ids)
          {
            upd_fnc(ivertex, estimate_count, current_block, current_tid, current_mat, 4);
            default_mesh_chunk = meshes[current_mat];
            default_ivertex = &ivertex[(long)default_mesh_chunk];
          }

          U1 = 0.0625f * (float) ( (current_tid - 1) % 16);
          U2 = U1 + 0.0625f;
          V1 = 0.0625f * (float) ( (current_tid - 1) / 16);
          V2 = V1 + 0.0625f;

          default_mesh_chunk->position (tmp_pos + Ogre::Vector3 (x,   y + 1, z));
          default_mesh_chunk->normal (0, 0, -1);
          default_mesh_chunk->textureCoord (U2, V1);
          default_mesh_chunk->position (tmp_pos + Ogre::Vector3 (x + 1, y + 1, z));
          default_mesh_chunk->normal (0, 0, -1);
          default_mesh_chunk->textureCoord (U1, V1);
          default_mesh_chunk->position (tmp_pos + Ogre::Vector3 (x + 1, y,   z));
          default_mesh_chunk->normal (0, 0, -1);
          default_mesh_chunk->textureCoord (U1, V2);
          default_mesh_chunk->position (tmp_pos + Ogre::Vector3 (x,   y,   z));
          default_mesh_chunk->normal (0, 0, -1);
          default_mesh_chunk->textureCoord (U2, V2);

          default_mesh_chunk->triangle ((*default_ivertex), (*default_ivertex) + 1, (*default_ivertex) + 2);
          default_mesh_chunk->triangle ((*default_ivertex) + 2, (*default_ivertex) + 3, (*default_ivertex));

          (*default_ivertex) += 4;
          visible = true;
        }

        //z+1
        block_tmp = default_block;
        if (z < chunk_size - 1) block_tmp = map[x][y][z + 1];
        else block_tmp = mprt->get_near_block (this, x, y, z + 1);

        if (mprt->is_displayable(block_tmp))
        {
          if (has_texture_ids)
          {
            upd_fnc(ivertex, estimate_count, current_block, current_tid, current_mat, 5);
            default_mesh_chunk = meshes[current_mat];
            default_ivertex = &ivertex[(long)default_mesh_chunk];
          }

          U1 = 0.0625f * (float) ( (current_tid - 1) % 16);
          U2 = U1 + 0.0625f;
          V1 = 0.0625f * (float) ( (current_tid - 1) / 16);
          V2 = V1 + 0.0625f;

          default_mesh_chunk->position (tmp_pos + Ogre::Vector3 (x,   y,   z + 1));
          default_mesh_chunk->normal (0, 0, 1);
          default_mesh_chunk->textureCoord (U1, V2);
          default_mesh_chunk->position (tmp_pos + Ogre::Vector3 (x + 1, y,   z + 1));
          default_mesh_chunk->normal (0, 0, 1);
          default_mesh_chunk->textureCoord (U2, V2);
          default_mesh_chunk->position (tmp_pos + Ogre::Vector3 (x + 1, y + 1, z + 1));
          default_mesh_chunk->normal (0, 0, 1);
          default_mesh_chunk->textureCoord (U2, V1);
          default_mesh_chunk->position (tmp_pos + Ogre::Vector3 (x,   y + 1, z + 1));
          default_mesh_chunk->normal (0, 0, 1);
          default_mesh_chunk->textureCoord (U1, V1);

          default_mesh_chunk->triangle ((*default_ivertex), (*default_ivertex) + 1, (*default_ivertex) + 2);
          default_mesh_chunk->triangle ((*default_ivertex) + 2, (*default_ivertex) + 3, (*default_ivertex));

          (*default_ivertex) += 4;
          visible = true;
        }

        auto i = mprt->get_special_block_hook (current_block);

        if (visible && i != nullptr) // the block is visible
          i->on_visible (current_block, Ogre::Vector3 (x, y, z) + tmp_pos);
        else if (i != nullptr) // the block is hidden
          i->on_hidden (current_block, Ogre::Vector3 (x, y, z) + tmp_pos);
      }
    }
  }

  for (auto &i : meshes) // end
    i.second->end();

}

void cr::chunk_map::chunk::cleanup()
{
  for (auto &i : meshes) // cleanup
  {
    root->detachObject (i.second);
    delete i.second;
  }
  meshes.clear();
  dirty = true;
  Ogre::Vector3 tmp_pos = position * chunk_size;
  for (size_t x = 0; x < cr::chunk_map::chunk_size; ++x)
  {
    for (size_t y = 0; y < cr::chunk_map::chunk_size; ++y)
    {
      for (size_t z = 0; z < cr::chunk_map::chunk_size; ++z)
      {
        auto i = mprt->get_special_block_hook (map[x][y][z]);
        if (i != nullptr) // the block is hidden
          i->on_hidden (map[x][y][z], Ogre::Vector3 (x, y, z) + tmp_pos);
        map[x][y][z] = 0;
      }
    }
  }
}

// CHUNK_MAP CLASS
cr::chunk_map::chunk_map (const Ogre::Vector3 &cworld_size, const Ogre::String &cmaterial_name, Ogre::SceneManager *mgr)
  : map (0), smgr (mgr), root (0), world_size (cworld_size), wchk_size (cworld_size / chunk_size), material_name (cmaterial_name)
{
  if ( (size_t) world_size.x % chunk_size || (size_t) world_size.y % chunk_size || (size_t) world_size.x % chunk_size)
    throw bad_settings ("world size isn't a multiple of the chunk size  [FATAL]");

  map = new chunk[ (size_t) ( (world_size.x / chunk_size) * (world_size.y / chunk_size) * (world_size.z / chunk_size))];
  root = mgr->getRootSceneNode()->createChildSceneNode();

  for (Ogre::Vector3 i (0, 0, 0); i.x < wchk_size.x; ++i.x)
    for (i.y = 0; i.y < wchk_size.y; ++i.y)
      for (i.z = 0; i.z < wchk_size.z; ++i.z)
        map[ (size_t) (i.x + i.y * wchk_size.x + i.z * wchk_size.x * wchk_size.y)].init (root, i, this);
}

cr::chunk_map::~chunk_map()
{
  smgr->getRootSceneNode()->removeChild (root);
  delete [] map;
  delete root;
}

void cr::chunk_map::cleanup()
{
  for (Ogre::Vector3 i (0, 0, 0); i.x < wchk_size.x; ++i.x)
    for (i.y = 0; i.y < wchk_size.y; ++i.y)
      for (i.z = 0; i.z < wchk_size.z; ++i.z)
        map[ (size_t) (i.x + i.y * wchk_size.x + i.z * wchk_size.x * wchk_size.y)].cleanup();
  update();
}

void cr::chunk_map::set_special_blocks (std::initializer_list< std::pair< const block, cr::special_block * > > cspecb)
{
  special_blocks = decltype(special_blocks)(cspecb);
}

void cr::chunk_map::set_additional_material(const Ogre::String &mat_name, std::initializer_list< cr::chunk_map::block > blocks, bool is_transparent)
{
  for (block b : blocks)
  {
    block_material[b] = mat_name;
    block_material_type[b] = is_transparent;
  }
}

void cr::chunk_map::remove_additional_material(const Ogre::String &mat_name)
{
  std::list<block> todel;
  for (auto &i : block_material)
  {
    if (i.second == mat_name)
      todel.push_back(i.first);
  }
  for (block i : todel)
    remove_additional_material(i);
}

inline void cr::chunk_map::remove_additional_material(cr::chunk_map::block b)
{
  block_material.erase(b);
  block_material_type.erase(b);
}

void cr::chunk_map::set_block_textures(cr::chunk_map::block bck, std::initializer_list< cr::chunk_map::block > textures_ids)
{
  std::array<block, 6> tmp;

  block last = bck;
  size_t cnt = 0;
  for (block idx : textures_ids)
  {
    tmp[cnt++] = idx;
    last = idx;
  }
  if (!cnt) // don't wast our time/memory here.
    return;
  while (cnt < 6)
    tmp[cnt++] = last;
  block_texture_ids[bck] = tmp;
}

cr::chunk_map::block &cr::chunk_map::get_block (const Ogre::Vector3 &pos)
{
  chunk *c = 0;
  if (! (c = get_chunk_for_point (pos)))
    throw out_of_range ("get_block pos is out of range: " + Ogre::StringConverter::toString(pos));
  return c->map[ (size_t) pos.x % chunk_size][ (size_t) pos.y % chunk_size][ (size_t) pos.z % chunk_size];
}

cr::chunk_map::block cr::chunk_map::get_block_copy (const Ogre::Vector3 &pos) const
{
  chunk *c = 0;
  if (! (c = get_chunk_for_point (pos)))
    return 0;
  return c->map[ (size_t) pos.x % chunk_size][ (size_t) pos.y % chunk_size][ (size_t) pos.z % chunk_size];
}

void cr::chunk_map::set_block (cr::chunk_map::block b, const Ogre::Vector3 &pos)
{
  chunk *c = 0;
  if (! (c = get_chunk_for_point (pos)))
    throw out_of_range ("set_block pos is out of range: " + Ogre::StringConverter::toString(pos));
  block &cb = c->map[ (size_t) pos.x % chunk_size][ (size_t) pos.y % chunk_size][ (size_t) pos.z % chunk_size];
  if (special_blocks.count(cb))
    special_blocks[cb]->on_hidden(cb, Ogre::Vector3((long)pos.x, (long)pos.y, (long)pos.z));
  cb = b;
  c->set_dirty();

  // update around chunks if needed (else there's missing faces)
  if ((size_t)pos.x % chunk_size == 0 && pos.x)
    set_dirty(Ogre::Vector3(pos.x - 1, pos.y, pos.z));
  if ((size_t)pos.y % chunk_size == 0 && pos.y)
    set_dirty(Ogre::Vector3(pos.x, pos.y - 1, pos.z));
  if ((size_t)pos.z % chunk_size == 0 && pos.z)
    set_dirty(Ogre::Vector3(pos.x, pos.y, pos.z - 1));
  if ((size_t)pos.x % chunk_size == chunk_size - 1 && pos.x != world_size.x - 1)
    set_dirty(Ogre::Vector3(pos.x + 1, pos.y, pos.z));
  if ((size_t)pos.y % chunk_size == chunk_size - 1 && pos.y != world_size.y - 1)
    set_dirty(Ogre::Vector3(pos.x, pos.y + 1, pos.z));
  if ((size_t)pos.z % chunk_size == chunk_size - 1 && pos.z != world_size.z - 1)
    set_dirty(Ogre::Vector3(pos.x, pos.y, pos.z + 1));
}

void cr::chunk_map::set_dirty (const Ogre::Vector3 &pos)
{
  chunk *c = 0;
  if (! (c = get_chunk_for_point (pos)))
    throw out_of_range ("get_block pos is out of range");
  c->set_dirty();
}

void cr::chunk_map::update()
{
  for (size_t i = 0; i < wchk_size.x * wchk_size.y * wchk_size.z; ++i)
    map[i].update();
}

int cr::chunk_map::get_ground_level (int x, int z)
{
  for (int y = wchk_size.y - 1; y >= 0 ; --y)
  {
    chunk *c = get_chunk_for_point (Ogre::Vector3 (x, y * chunk_size, z));
    if (!c)
      return (0);
    for (int ry = chunk_size - 1; ry >= 0; --ry)
    {
      if (c->map[x % chunk_size][ry][z % chunk_size])
        return (ry + y * chunk_size);
    }
  }
  return (0);
}

bool cr::chunk_map::is_ground (int x, int y, int z)
{
  chunk *cc = get_chunk_for_point (Ogre::Vector3 (x, y, z));
  chunk *cu = get_chunk_for_point (Ogre::Vector3 (x, y + 1, z));
  // lower boundary check
  if (!cc && cu && !cu->map[x % chunk_size][ (y + 1) % chunk_size][z % chunk_size])
    return true;
  if (!cc)
    return false;
  // upper boundary check
  if (!cu && cc->map[x % chunk_size][ (y) % chunk_size][z % chunk_size])
    return true;
  if (!cu)
    return false;
  // inside boundaries check
  if (cc->map[x % chunk_size][ (y) % chunk_size][z % chunk_size]
      && !cu->map[x % chunk_size][ (y + 1) % chunk_size][z % chunk_size])
    return true;
  return false;
}

int cr::chunk_map::get_upper_ground_level (int x, int y, int z)
{
  chunk *tc = get_chunk_for_point (Ogre::Vector3 (x, y, z));
  if (tc && !tc->map[x % chunk_size][y % chunk_size][z % chunk_size])
    return (-1); // ground is lower
  if (!tc)
    return (0);
  for (int cy = y / chunk_size; cy < wchk_size.y; ++cy)
  {
    chunk *c = get_chunk_for_point (Ogre::Vector3 (x, cy * chunk_size, z));
    if (!c)
      return (0);
    for (int ry = (cy == y / chunk_size ? y % chunk_size : 0); ry < chunk_size; ++ry)
    {
      if (!c->map[x % chunk_size][ry][z % chunk_size])
        return (ry + cy * chunk_size - 1);
    }
  }
  return (-1); // ground is lower
}

#define sign(x) ((x) < 0 ? -1 : (x) > 0 ? 1 : 0)

Ogre::Vector3 cr::chunk_map::get_collision_cell (const Ogre::Vector3 &from, const Ogre::Vector3 &to, Ogre::Vector3 &normal, bool lastsafe)
{
  http://www.youtube.com/watch?v=Z-FOZ8WkqaE
  //http://www.xnawiki.com/index.php?title=Voxel_traversal

  // goto http;

  int x = from.x;
  int y = from.y;
  int z = from.z;

  const Ogre::Vector3 direction = (to - from);

  // Determine which way we go.
  int stepX = sign (direction.x);
  int stepY = sign (direction.y);
  int stepZ = sign (direction.z);

  Ogre::Vector3 cell_boundary (
    x + (stepX > 0 ? 1 : 0),
    y + (stepY > 0 ? 1 : 0),
    z + (stepZ > 0 ? 1 : 0));

  Ogre::Vector3 tMax (
    (cell_boundary.x - from.x) / direction.x,    // Boundary is a plane on the YZ axis.
    (cell_boundary.y - from.y) / direction.y,    // Boundary is a plane on the XZ axis.
    (cell_boundary.z - from.z) / direction.z);   // Boundary is a plane on the XY axis.

  if (std::isnan(tMax.x)) tMax.x = INFINITY;
  if (std::isnan(tMax.y)) tMax.y = INFINITY;
  if (std::isnan(tMax.z)) tMax.z = INFINITY;

  // Determine how far we must travel along the ray before we have crossed a gridcell.
  Ogre::Vector3 tDelta (
    (float)stepX / direction.x,                    // Crossing the width of a cell.
    (float) stepY / direction.y,                   // Crossing the height of a cell.
    (float) stepZ / direction.z);                  // Crossing the depth of a cell.
  if (std::isnan(tDelta.x)) tDelta.x = INFINITY;
  if (std::isnan(tDelta.y)) tDelta.y = INFINITY;
  if (std::isnan(tDelta.z)) tDelta.z = INFINITY;

  // For each step, determine which distance to the next voxel boundary is lowest (i.e.
  // which voxel boundary is nearest) and walk that way.
  const float mdst = (from.squaredDistance(to));
  float cdst = from.squaredDistance(Ogre::Vector3(x, y, z));
  size_t i = 0;
  while (cdst <= mdst + 1 && ++i < 160)
  {
    Ogre::Vector3 step = Ogre::Vector3::ZERO;
    if (tMax.x < tMax.y && tMax.x < tMax.z)
    {
      x += stepX;
      step.x = stepX;
      tMax.x += tDelta.x;
    }
    else if (tMax.y < tMax.z)
    {
      y += stepY;
      step.y = stepY;
      tMax.y += tDelta.y;
    }
    else
    {
      z += stepZ;
      step.z = stepZ;
      tMax.z += tDelta.z;
    }
    // check if the cell is free or not, if not, return immediatly
    try
    {
      if (get_block (Ogre::Vector3 (x, y, z)))
      {
        normal = (-step).normalisedCopy();
        if (lastsafe)
          return (Ogre::Vector3 (x, y, z) - step);
        else
          return Ogre::Vector3 (x, y, z);
      }
    }
    catch(...){} // out of map, probably. So, no collisions.
    cdst = from.squaredDistance(Ogre::Vector3(x, y, z));
  }

  return Ogre::Vector3(-1, -1, -1);
}

cr::chunk_map::block cr::chunk_map::get_near_block (cr::chunk_map::chunk *chk, int x, int y, int z)
{
  int t = (int) (size_t) ( (chk - map));
  // x + y * sx + z * sx * sy
  int pos[3] = {t % (int) wchk_size.x, (t / (int) wchk_size.x) % (int) wchk_size.y, t / (int) wchk_size.y / (int) wchk_size.x};
  if (x < 0)
  {
    --pos[0];
    x = chunk_size - 1;
  }
  if (x >= chunk_size)
  {
    ++pos[0];
    x = 0;
  }
  if (y < 0)
  {
    --pos[1];
    y = chunk_size - 1;
  }
  if (y >= chunk_size)
  {
    ++pos[1];
    y = 0;
  }
  if (z < 0)
  {
    --pos[2];
    z = chunk_size - 1;
  }
  if (z >= chunk_size)
  {
    ++pos[2];
    z = 0;
  }
  // no neighbourg chunk
  if (pos[0] < 0 || pos[0] >= wchk_size.x || pos[1] < 0 || pos[1] >= wchk_size.y
      || pos[2] < 0 || pos[2] >= wchk_size.z)
    return 1;
  return map[ (size_t) (pos[0] + pos[1] * wchk_size.x + pos[2] * wchk_size.x * wchk_size.y)].map[x][y][z];
}

// kate: indent-mode cstyle; indent-width 2; replace-tabs on; 
