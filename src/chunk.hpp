/*
 *  Copyright (C) 2012-2013 Timothée Feuillet
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Lesser General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Lesser General Public License for more details.
 *
 *  You should have received a copy of the GNU Lesser General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef __CHUNK_HPP__
#define __CHUNK_HPP__

#include <OgreRoot.h>
#include <OgreSceneManager.h>
#include <OgreSceneNode.h>
#include <OgreMaterial.h>
#include <OgreEntity.h>
#include <OgreManualObject.h>

#include <vector>
#include <array>
#include <boost/unordered_set.hpp>
#include <boost/unordered_map.hpp>

namespace cr
{
  // class hooked when its blocktype is visible
  class special_block
  {
    public:
      special_block() {}
      virtual ~special_block() {}

      special_block(const special_block &) = delete;
      special_block &operator = (const special_block &) = delete;

      // called when a block is visible (could be called multiple times for the same block)
      // bck : the block type
      // pos : the position of the block
      virtual void on_visible(unsigned char bck, const Ogre::Vector3 &pos) = 0; // I don't really care if its slow

      // WARNING: may be called one billion time. It must be ***VERY*** fast !!!
      // bck : the block type
      // pos : the position of the block
      virtual void on_hidden(unsigned char bck, const Ogre::Vector3 &pos) = 0;

    private:
  };

  //
  class chunk_map
  {
    public:
      using block = unsigned char;

      static const size_t chunk_size = 32; // 64: best value (best (render) perf)

    public:
      class out_of_range : public std::out_of_range
      {
        public:
          explicit out_of_range(const std::string &__arg) : std::out_of_range("chunk_map::out_of_range: " + __arg) {}
          virtual ~out_of_range() noexcept {}
      };

      class bad_settings : public std::runtime_error
      {
        public:
          explicit bad_settings(const std::string &__arg) : std::runtime_error("chunk_map::bad_settings: " + __arg) {}
          virtual ~bad_settings() noexcept {}
      };

    public:
      chunk_map(const Ogre::Vector3 &cworld_size, const Ogre::String &cmaterial_name, Ogre::SceneManager *mgr);
      chunk_map(const chunk_map &) = delete;
      ~chunk_map();

      chunk_map &operator = (const chunk_map &) = delete;

      void cleanup(); // empty the map

      // handler for special blocks
      void set_special_blocks(std::initializer_list<std::pair< const block, special_block *>> cspecb);

      // set different materials for some types of blocks
      void set_additional_material(const Ogre::String &mat_name, std::initializer_list<block> blocks, bool is_transparent = false);
      void remove_additional_material(const Ogre::String &mat_name); // slow
      void remove_additional_material(block t); // fast

      // alow block with different textures ids (ex. grass block)
      // if the number of textures_ids if < 6, the last is repeated.
      // [bottom, top, left, right, front, back]( [y-1, y+1, x-1, x+1, z-1, z+1]
      // the material applied to the face is the one of the texture id.
      void set_block_textures(block bck, std::initializer_list<block> textures_ids);

      // map accessors
      // those function could throw an chunk_map::out_of_range when out of range access
      block get_block_copy(const Ogre::Vector3 &pos) const; // get a copy of a block
      block &get_block(const Ogre::Vector3 &pos); // get a block. NOTE: you shall call set_dirty yourself

      void set_block(block b, const Ogre::Vector3 &pos);

      // NOTICE: after any modifications, don't forget a call to update !!

      // utility functions

      // return the maximum y that could be found for [x,z]
      int get_ground_level(int x, int z);

      // return true if the map at [x,y,z] and [x,y+1,z] has this configuration:
      // [air   block] <-- x,y + 1,z
      // [solid block] <-- x,y,z     --> this is the ground level
      bool is_ground(int x, int y, int z);

      // get the upper ground level for the block [x,y,z] (could be used to re-set the player position)
      int get_upper_ground_level(int x, int y, int z);

      // return Ogre::Vector3(-1, -1, -1) when there isn't any collisions
      // else return the collision cell (add (0.5, 0.5, 0.5) to get the center of the cell)
      // normal is the normal of the face where the ray intersect
      // lastsafe allow to get the position of the cell just before the ray intersect.
      Ogre::Vector3 get_collision_cell(const Ogre::Vector3 &from, const Ogre::Vector3 &to, Ogre::Vector3 &normal, bool lastsafe = true);

      // update the dirty chunks
      // (apply the changes)
      // NOTE: you must call this after the constructor !!
      void update();
      // set a chunk as dirty (the pos is within a chunk, or this will throw)
      void set_dirty(const Ogre::Vector3 &pos);

      Ogre::SceneNode *get_root_scene_node()
      {
        return root;
      }

    private: // for use in subclass chunk
      inline bool is_transparent(block bnum)
      {
        return block_material_type.count(bnum) && block_material_type.at(bnum);
        //return transparent_blocks.count (bnum);
      }

      inline bool is_displayable(block bnum)
      {
        return !bnum || is_transparent(bnum);
      }

      inline Ogre::String get_material_name(block bnum)
      {
        return (!block_material.count(bnum) ? material_name : block_material.at(bnum));
      }

      inline block get_textures_ids(block bnum, uint8_t facenum)
      {
        return block_texture_ids.count(bnum) ? block_texture_ids.at(bnum)[facenum] : bnum;
      }

      inline bool has_textures_ids(block bnum)
      {
        return block_texture_ids.count(bnum);
      }

      inline special_block *get_special_block_hook(block bnum)
      {
        if (special_blocks.count(bnum))
          return special_blocks.at(bnum);
        return nullptr;
      }

    private:
      class chunk
      {
        public:
          chunk();
          ~chunk();
          void init(Ogre::SceneNode *root, const Ogre::Vector3 &pos, cr::chunk_map *d);

          bool is_empty() const
          {
            return empty;
          }
          bool is_dirty() const
          {
            return dirty;
          }
          void set_dirty(bool d = true)
          {
            dirty = d;
          }

          void update();
          void cleanup();

        private:
          void upd_fnc(boost::unordered_map<long, int> &ivertex, size_t estimate_count, block current_block, block &current_tid, Ogre::String &current_mat, block idx);

        public: // fuck the getters / setters
          block map[cr::chunk_map::chunk_size][cr::chunk_map::chunk_size][cr::chunk_map::chunk_size]; // fuck 1D arrays
          // fuck the heap


        private:
          Ogre::SceneNode *root;
          chunk_map *mprt;
          bool dirty;
          bool empty;

          // key is material name
          boost::unordered_map<Ogre::String, Ogre::ManualObject *> meshes;

          Ogre::Vector3 position; // the base voxels position
      };

    private:
      inline chunk *get_chunk_for_point(const Ogre::Vector3 &p) const
      {
        if (p.x < 0 || p.x >= world_size.x || p.y < 0 || p.y >= world_size.y || p.z < 0 || p.z >= world_size.z)
          return (0);
        Ogre::Vector3 tmp = p / chunk_size;

        return map + (size_t)((size_t)(tmp.x) + (size_t)(tmp.y) * (wchk_size.x)
                              + (size_t) tmp.z * (wchk_size.y * wchk_size.x));
      }

      block get_near_block(chunk *chk, int x, int y, int z);

    private:
      Ogre::SceneNode *root;
      chunk *map;
      Ogre::SceneManager *smgr;

      Ogre::Vector3 world_size;
      Ogre::Vector3 wchk_size;
      Ogre::String material_name;

      // boost implementation is faster than the one in libstdc++
      boost::unordered_map<block, special_block *> special_blocks; // fatser than an array search (worst case is linear)
      boost::unordered_map<block, Ogre::String> block_material; // additional materials
      boost::unordered_map<block, bool> block_material_type; // additional materials: transparency
      boost::unordered_map<block, std::array<block, 6>> block_texture_ids;
  };
} // namespace cr

#endif /*__CHUNK_HPP__*/

#include "astar.hpp"
// kate: indent-mode cstyle; indent-width 2; replace-tabs on; 
