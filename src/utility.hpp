/*
 *  Copyright (C) 2012-2013 Timothée Feuillet
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Lesser General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Lesser General Public License for more details.
 *
 *  You should have received a copy of the GNU Lesser General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
//
// file : utility.hpp
// in : file:///home/feuill_t/project_y3/cr::map/src/utility.hpp
//
// created by : Timothée Feuillet on fus-ro-dah.site
// date: 06/03/2013 21:53:43
//

#ifndef __UTILITY_HPP4242__
#define __UTILITY_HPP4242__

#include <cmath>
#include <OgreVector3.h>
#include <OgreMovableObject.h>
#include <OgreSceneNode.h>
#include <OgreSceneManager.h>

// vector comp function
struct vector_cmp
{
  bool operator()(const Ogre::Vector3 &mine, const Ogre::Vector3 &other) const
  {
    if (mine.x != other.x)
      return (mine.x < other.x);

    if (mine.y != other.y)
      return (mine.y < other.y);

    return (mine.z < other.z);
  }
  size_t operator()(const Ogre::Vector3 &hash) const
  {
    unsigned long *hsh[] =
    {
      (unsigned long *)(&hash.x),
      (unsigned long *)(&hash.y),
      (unsigned long *)(&hash.z)
    };
    return ((*(hsh[0])) << (4 * 8)) ^ ((*(hsh[1])) << (2 * 8)) ^ (*(hsh[2]));
  }
};

static long double operator "" _fps(long double val)
{
  return 1.0 / val;
}

static long double operator "" _rand(long double val)
{
  return (static_cast<float>(rand()) / static_cast<float>(RAND_MAX)) * val;
}

static Ogre::Vector3 operator "" _x(long double val)
{
  return Ogre::Vector3(val, 0, 0);
}
static Ogre::Vector3 operator "" _y(long double val)
{
  return Ogre::Vector3(0, val, 0);
}
static Ogre::Vector3 operator "" _z(long double val)
{
  return Ogre::Vector3(0, 0, val);
}

// http://www.ogre3d.org/forums/viewtopic.php?f=2&t=53647&start=0
static void destroy_all_attached_movable_objects(Ogre::SceneNode *i_pSceneNode)
{
  if (!i_pSceneNode)
    return;

  // Destroy all the attached objects
  Ogre::SceneNode::ObjectIterator itObject = i_pSceneNode->getAttachedObjectIterator();

  while (itObject.hasMoreElements())
  {
    Ogre::MovableObject *pObject = static_cast<Ogre::MovableObject *>(itObject.getNext());
    i_pSceneNode->getCreator()->destroyMovableObject(pObject);
  }

  // Recurse to child SceneNodes
  Ogre::SceneNode::ChildNodeIterator itChild = i_pSceneNode->getChildIterator();

  while (itChild.hasMoreElements())
  {
    Ogre::SceneNode *pChildNode = static_cast<Ogre::SceneNode *>(itChild.getNext());
    destroy_all_attached_movable_objects(pChildNode);
  }
}

#endif /*__UTILITY_HPP__*/
// kate: indent-mode cstyle; indent-width 2; replace-tabs on; 
